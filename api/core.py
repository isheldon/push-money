from flask import Blueprint, jsonify, request, url_for

from api.logic.core import generate_and_save_wallet, get_address_balance, get_spend_categories, spend_balance
from api.models import PushWallet
from minter.helpers import create_deeplink

bp_api = Blueprint('api', __name__, url_prefix='/api')


HTTP_400_BAD_REQUEST = 400
HTTP_401_UNAUTHORIZED = 401
HTTP_404_NOT_FOUND = 404
HTTP_500_INTERNAL_SERVER_ERROR = 500


@bp_api.route('/', methods=['GET'])
def health():
    return f'Api ok. <a href={url_for("root.readme")}>Guide</a>'


@bp_api.route('/push/create', methods=['POST'])
def push_create():
    payload = request.get_json() or {}
    sender, recipient = payload.get('sender'), payload.get('recipient')
    password = payload.get('password')
    amount = payload.get('amount')

    wallet = generate_and_save_wallet(sender, recipient, password)
    response = {
        'address': wallet.address,
        'link_id': wallet.link_id
    }
    if amount:
        response['deeplink'] = create_deeplink(wallet.address, amount)
    return jsonify(response)


@bp_api.route('/push/<link_id>/info', methods=['GET'])
def push_info(link_id):
    wallet = PushWallet.get_or_none(link_id=link_id)
    if not wallet:
        return jsonify({'error': 'Link does not exist'}), HTTP_404_NOT_FOUND

    return jsonify({
        'sender': wallet.sender,
        'recipient': wallet.recipient,
        'is_protected': wallet.password_hash is not None
    })


@bp_api.route('/push/<link_id>/balance', methods=['GET', 'POST'])
def push_balance(link_id):
    payload = request.get_json() or {}
    password = payload.get('password')

    wallet = PushWallet.get_or_none(link_id=link_id)
    if not wallet:
        return jsonify({'error': 'Link does not exist'}), HTTP_404_NOT_FOUND

    if not wallet.auth(password):
        return jsonify({'error': 'Incorrect password'}), HTTP_401_UNAUTHORIZED

    balance = get_address_balance(wallet.address)
    response = {
        'address': wallet.address,
        **balance
    }
    return jsonify(response)


@bp_api.route('/spend/list', methods=['GET'])
def spend_options():
    categories = get_spend_categories()
    return jsonify(categories)


@bp_api.route('/spend/<link_id>', methods=['POST'])
def make_spend(link_id):
    payload = request.get_json() or {}
    password = payload.get('password')

    wallet = PushWallet.get_or_none(link_id=link_id)
    if not wallet:
        return jsonify({'error': 'Link does not exist'}), HTTP_404_NOT_FOUND

    if not wallet.auth(password):
        return jsonify({'error': 'Incorrect password'}), HTTP_401_UNAUTHORIZED

    if 'option' not in payload:
        return jsonify({'error': '"option" key is required'}), HTTP_400_BAD_REQUEST
    allowed_options = ['mobile', 'transfer-minter']
    if payload['option'] not in allowed_options:
        return jsonify({
            'error': f'Allowed options are: {",".join(option for option in allowed_options)}'
        }), HTTP_400_BAD_REQUEST

    success = spend_balance(wallet, payload['option'], **payload.get('params', {}))
    if not success:
        return jsonify({'error': 'Internal API error'}), HTTP_500_INTERNAL_SERVER_ERROR

    return jsonify({'message': 'Success'})
